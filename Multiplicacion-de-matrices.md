# Multiplicacion de matrices en Python

_A continuacion presento el algoritmo y el analisis del mismo, para el siguiente algoritmo use Google Colab para generar el analisis en ese entorno y ver como se comporta_

## Script

```
import numpy as np
import time

# Set the size of the matrices
matrix_size = 1000

# Generate random matrices
matrix1 = np.random.rand(matrix_size, matrix_size)
matrix2 = np.random.rand(matrix_size, matrix_size)

# Multiply matrices using NumPy
start_time = time.time()
result_numpy = np.matmul(matrix1, matrix2)
end_time = time.time()
numpy_time = (end_time - start_time) * 1000

# Multiply matrices using Python lists
start_time = time.time()
result_python = [[sum(a * b for a, b in zip(X_row, Y_col)) for Y_col in zip(*matrix2)] for X_row in matrix1]
end_time = time.time()
python_time = (end_time - start_time) * 1000

# Print results
print("NumPy multiplication took:", numpy_time, "milliseconds")
print("Python list multiplication took:", python_time, "milliseconds")

# Print the difference in execution time
print("Difference in execution time:", python_time - numpy_time, "milliseconds")
```

> Este es el script que pude generar con Google Colab despues de algunas pruebas con diferentes prompts, de esta manera puedo optener los resultados y encontrar el analisis apropiado para esta parte del taller.

## Resultados por CPU

_El resultado optenido del algoritmo ejecutando la multiplicacion de matrices en numpy tanto como en python es el siguiente_

```
NumPy multiplication took: 74.57566261291504 milliseconds
Python list multiplication took: 310039.6304130554 milliseconds
Difference in execution time: 309965.0547504425 milliseconds
```

## Resultados por GPU

_Ahora vamos a ver el resultado usando el entorno con GPU, estos fueron los resultados del script_

```
NumPy multiplication took: 79.15258407592773 milliseconds
Python list multiplication took: 273680.07922172546 milliseconds
Difference in execution time: 273600.92663764954 milliseconds
```

## Resultados por TPU

_Continuamos con los resultados tomando como base en las unidades de Tensores TPU, los resultados fueron los siguientes_

```
NumPy multiplication took: 127.73919105529785 milliseconds
Python list multiplication took: 333187.32237815857 milliseconds
Difference in execution time: 333059.5831871033 milliseconds
```

# Uso de Tensores con Tensorflow

## Algoritmo en Tensorflow

**El algoritmo para generar la multiplicacion de los tensores es el siguiente.**

```
import time
import tensorflow as tf


# Create two large tensors
tensor_a = tf.random.uniform(shape=(100, 1000, 1000))
tensor_b = tf.random.uniform(shape=(100, 1000, 1000))

# Start the timer
start_time = time.time()

# Multiply the tensors using matrix multiplication
product_tensor = tf.math.multiply(tensor_a, tensor_b)

# Stop the timer and calculate the elapsed time
end_time = time.time()
elapsed_time = end_time - start_time

# Print the elapsed time
print(product_tensor)

print(f"Elapsed time: {elapsed_time:.2f} seconds")
```

Es necesario destacar que el tamaño de los tensores fue reducido ya que originalmente lo cree de este tamaño (100, 1000, 10000) pero no fue posible ejecutarlo por ninguno de las unidades de procesamiento, ya que las medidas de los tensores generaban un problema de complejidad espacial, ni siquiera fue posible ejecutarlo por TPU. Es por eso que modifique su medida a (100, 1000, 1000)

> Para los resultados solo pondre el tiempo de ejecicion, sin embargo el resultado del tensor puede ser visto en el Colab que estara adjunto al final.

## Resultado por CPU

```
Elapsed time: 0.34 seconds
```

## Resultado por GPU

```
Elapsed time: 0.33 seconds
```

## Resultado por TPU

```
Elapsed time: 0.24 seconds
```

#### Cuadernos en Colab

[Google Colab Matrices](https://colab.research.google.com/drive/1XNL-aZSV7d-n0Jvs_H0EYMp-3ozZ4KHX?usp=sharing)

[Google Colab Tensores](https://colab.research.google.com/drive/16_x7TyQhihTQJEPtviDb10qmd98S47GK?usp=sharing)